<?php

use allspark\JWT\JWT;
function auth()
{
	global $input, $sha_key;
	
	if (isset($_COOKIE['token']))
		$token = $_COOKIE['token'];
	else
	{
		http_response_code(401);
		die(json_encode(array("code" => 401, "message" => "Accès refusé - Token absent")));
	}

	try
	{
		$payload = (new JWT($sha_key, 'HS512', 3600, 10))->decode($token);
		$input->user = $payload['user'];
	}
	catch (Throwable $e)
	{
		http_response_code(401);
		die(json_encode(array("code" => 401, "message" => "Accès refusé - " . $e->getMessage())));
	}
}


function is_allowed_origin($allowed_origins = NULL)
{
	global $connection;
	if ($allowed_origins == NULL)
		$allowed_origins = $connection->query("SELECT origin FROM server.allowed_origins")->fetch(PDO::FETCH_NUM);
	$origin = isset($_SERVER['HTTP_ORIGIN']) ? $_SERVER['HTTP_ORIGIN'] : $_SERVER['SERVER_NAME'];
	$origin_without_protocol = substr($origin,strpos($origin,'://')+3);
	if (is_array($allowed_origins) AND (in_array('*',$allowed_origins) OR in_array($origin_without_protocol,$allowed_origins) OR in_array('*.'.substr($origin_without_protocol,strpos($origin_without_protocol,'.')+1),$allowed_origins)))
		return true;
	else
		return false;
}


function allowed_origins_only($allowed_origins = NULL)
{
	global $connection;
	if ($allowed_origins == NULL)
		$allowed_origins = array_column($connection->query("SELECT origin FROM server.allowed_origins")->fetchAll(PDO::FETCH_NUM),0);
	if (!is_allowed_origin($allowed_origins))
	{
		http_response_code(403);
		die(json_encode(array("code" => 403, "message" => "Accès refusé - Les connexions depuis cette origine ne sont pas autorisées")));
	}
}


function is_admin($user_id)
{
	global $connection;
	$isadmin = $connection->prepare("SELECT `admin` FROM `server`.`users` WHERE id = :id AND status = 1");
	$isadmin->bindParam(':id', $user_id);
	$isadmin->execute();
	$isadmin = $isadmin->fetch(PDO::FETCH_ASSOC);
	if ($isadmin['admin'] == 1)
		return true;
	else
		return false;
}


function admin_only()
{
	global $input;
	if (!is_admin($input->user->id))
	{
		http_response_code(403);
		die(json_encode(array("code" => 403, "message" => "Accès refusé - Cette méthode est réservée aux administrateurs")));
	}
}


function exists($connection, $db, $table, $field, $value)
{
	$exists = $connection->prepare("SELECT " . $field . " FROM `" . $db . "`.`" . $table . "` WHERE " . $field . " = :" . $field);
	$exists->bindParam(':'.$field, $value);
	$exists->execute();
	if ($exists->rowCount() == 0)
		return false;
	else
		return true;
}


function validate($name, $value, $format, $required = false)
{
	global $input;

	if (isset($value) && $value!='' && isset($format))
		if ($format == 'topdomain' AND !preg_match("/^([a-z0-9][a-z0-9-]*[a-z0-9]\.)([a-z0-9][a-z0-9-]*[a-z0-9])$/", $value)) // web.network
			$return = array("code" => 400, "message" => $name . ' : ' . $value . " n'est pas un domaine de premier niveau valide");
		else if ($format == 'domain' AND !preg_match("/^([a-z0-9][a-z0-9-]*[a-z0-9]\.)+([a-z0-9][a-z0-9-]*[a-z0-9])$/", $value)) // web.network  test.web.network
			$return = array("code" => 400, "message" =>  $name . ' : ' . $value . " n'est pas un domaine valide");
		else if ($format == 'email' AND !preg_match("/^(([a-z0-9][a-z0-9-_.]*[a-z0-9])+@([a-z0-9][a-z0-9-]*[a-z0-9]\.)+([a-z0-9][a-z0-9-]*[a-z0-9]))$/", $value)) // test_6@test.network  test-8@web.test.network
			$return = array("code" => 400, "message" =>  $name . ' : ' . $value . " n'est pas une adresse email valide");
		else if ($format == 'email_list' AND !preg_match("/^(([a-z0-9][a-z0-9-_.]*[a-z0-9])+@([a-z0-9][a-z0-9-]*[a-z0-9]\.)+([a-z0-9][a-z0-9-]*[a-z0-9]);?)+$/", $value)) // first@test.org;second@test.org;third@test.org
			$return = array("code" => 400, "message" =>  $name . ' : ' . $value . " n'est pas une liste d'adresses email valide");
		else if ($format == 'origin' AND !preg_match("/^(\*)$|^(localhost|(\*\.)?([a-z0-9][a-z0-9-]*[a-z0-9]\.)+[a-z0-9][a-z0-9-]*[a-z0-9]|(2[0-4]\d|25[0-5]|1\d\d|\d\d?)\.(?5)\.(?5)\.(?5))(:((6553[0-5])|(655[0-2][0-9])|(65[0-4][0-9]{2})|(6[0-4][0-9]{3})|([1-5][0-9]{4})|([0-5]{0,5})|([0-9]{1,4})))?$/", $value)) // *  localhost:8080  *.optimus-avocats.fr  www.optimus-avocats.fr:9091  192.168.0.5:3232
			$return = array("code" => 400, "message" =>  $name . ' : ' . $value . " n'est pas une origine valide");
		else if ($format == 'ipv4' AND !preg_match("/^(2[0-4]\d|25[0-5]|1\d\d|\d\d?)\.(?1)\.(?1)\.(?1)$/", $value)) // 192.168.0.5
			$return = array("code" => 400, "message" =>  $name . ' : ' . $value . " n'est pas une adresse ip valide");
		else if ($format == 'integer' AND !preg_match("/^-?\d+$/", $value)) // 1215
			$return = array("code" => 400, "message" =>  $name . ' : ' . $value . " n'est pas un nombre entier valide");
		else if ($format == 'decimal' AND !preg_match("/^-?(\d*\.)?\d+$/", $value)) // 44.59
			$return = array("code" => 400, "message" =>  $name . ' : ' . $value . " n'est pas un nombre decimal valide");
		else if ($format == 'boolean' AND !preg_match("/^[01]$/", $value))
			$return = array("code" => 400, "message" =>  $name . ' : ' . $value . " n'est pas un nombre boolean valide. Valeurs autorisées : 0 ou 1");
		else if ($format == 'string' AND !preg_match("/^[\w\t \-ÊÉÈœàâäéèêëïîôöùûüÿç().,?!\"\'+=+\-=\$\€\/_*@&~#%:;°]+$/D", $value))
			$return = array("code" => 400, "message" =>  $name . ' : ' . $value . " n'est pas une chaîne valide");
		else if ($format == 'text' AND !preg_match("/^[\w \-ÊÉÈœàâäéèêëïîôöùûüÿç().,?!\"\'+\-=\$\€_*@&~#%:;°\r\n]+$/", $value))
			$return = array("code" => 400, "message" =>  $name . ' : ' . $value . " n'est pas une chaîne multiligne valide");
		else if ($format == 'filename' AND ($value=='.' OR $value=='..' OR substr($value,0,1)==' ' OR substr($value,-1)==' ' OR !preg_match("/^[a-zA-Z0-9 ._@\-àâäéèêëïîôöùûüÿç()\']+$/", $value)))
			$return = array("code" => 400, "message" =>  $name . ' : ' . $value . " n'est pas un nom de fichier valide");
		else if ($format == 'date' AND $value != '' AND !preg_match("/^(((\d\d)(([02468][048])|([13579][26]))-02-29)|(((\d\d)(\d\d)))-((((0\d)|(1[0-2]))-((0\d)|(1\d)|(2[0-8])))|((((0[13578])|(1[02]))-31)|(((0[1,3-9])|(1[0-2]))-(29|30)))))$/", $value)) // 2021-08-31
			$return = array("code" => 400, "message" =>  $name . ' : ' . $value . " n'est pas une date valide");
		else if ($format == 'datetime' AND !preg_match("/^(((\d\d)(([02468][048])|([13579][26]))-02-29)|(((\d\d)(\d\d)))-((((0\d)|(1[0-2]))-((0\d)|(1\d)|(2[0-8])))|((((0[13578])|(1[02]))-31)|(((0[1,3-9])|(1[0-2]))-(29|30)))))\s(([01]\d|2[0-3]):([0-5]\d):([0-5]\d))$/", $value)) // 2020-02-29 00:00:00
			$return = array("code" => 400, "message" =>  $name . ' : ' . $value . " n'est pas une date/heure valide");
		else if ($format == 'time' AND !preg_match("/^(([01]\d|2[0-3]):([0-5]\d):([0-5]\d))$/", $value)) // 00:00:00
			$return = array("code" => 400, "message" =>  $name . ' : ' . $value . " n'est pas une heure valide");
		else if ($format == 'resource' AND $value!='*' AND !preg_match("/^[a-z0-9@\.\/]+\*?$/", $value))
			$return = array("code" => 400, "message" =>  $name . ' : ' . $value . " n'est pas une ressource valide");
		else if ($format == 'module' AND !preg_match("/^[a-z0-9-_]+$/", $value))
			$return = array("code" => 400, "message" =>  $name . ' : ' . $value . " n'est pas un module valide");
		else if ($format == 'hexcolor' AND !preg_match("/^(?:[0-9a-fA-F]{3}){1,2}$/", $value))
			$return = array("code" => 400, "message" =>  $name . ' : ' . $value . " n'est pas une couleur hexadécimale valide");
		elseif ($format == 'password')
		{
			if (!preg_match('@[A-Z]@', $value)) 
				$return = array("code" => 400, "message" =>  $name . " : Le mot de passe doit contenir au moins une lettre majuscule");
			if (!preg_match('@[a-z]@', $value)) 
				$return = array("code" => 400, "message" =>  $name . " : Le mot de passe doit contenir au moins une lettre minuscule");
			if (!preg_match('@[0-9]@', $value)) 
				$return = array("code" => 400, "message" =>  $name . " : Le mot de passe doit contenir au moins un chiffre");
			if (!preg_match('@[^\w]@', $value)) 
				$return = array("code" => 400, "message" =>  $name . " : Le mot de passe doit contenir au moins un caractère spécial");
			if (strlen($value) < 9) 
				$return = array("code" => 400, "message" =>  $name . " : Le mot de passe doit contenir au moins 9 caractères");
		}
		else if ($format == 'json')
		{
			json_decode($value);
			if (json_last_error() != JSON_ERROR_NONE)
				$return = array("code" => 400, "message" =>  $name . " : n'est pas une chaine JSON valide");
		}
		else if ($format == 'locked' AND isset($value))
			$return = array("code" => 400, "message" =>  $name . " : ne peut pas être modifié");
		
		else if (!in_array($format,array('topdomain','domain','email','email_list','origin','ipv4','integer','decimal','boolean','string','text','filename','date','datetime','time','resource','module','locked','password','json','hexcolor')))
			$return = array("code" => 400, "message" =>  $name . ' : ' . $format . " n'est pas un format valide");

			
	if ($required AND !isset($value))
		$return = array("code" => 400, "message" => $name . " doit obligatoirement être renseigné");

	if ($format == 'boolean')
		$input->fields[$name] = PDO::PARAM_BOOL;
	else if ($format == 'integer')
		$input->fields[$name] = PDO::PARAM_INT;
	else
		$input->fields[$name] = PDO::PARAM_STR;

	if (isset($value) AND $value == '')
		if (in_array($format, array('topdomain','domain','email','email_list','origin','ipv4','string','text','date','datetime','resource','module')))
			$input->body->$name = null;
		else if (in_array($format, array('integer','decimal','boolean')))
			$input->body->$name = 0;
		else if (in_array($format, array('filename','password','json')))
			$return = array("code" => 400, "message" => $name . " ne peut pas être vide");

	if (@$return)
	{
		http_response_code($return['code']);
		die(json_encode($return));
	}
	else
		return true;
}


function get_rights($user_id, $owner_id, $resource)
{
	global $connection;
	if ($user_id === $owner_id OR is_admin($user_id))
		return array('read' => '1', 'write' => '1', 'create' => '1', 'delete' => '1');

	$resources_array = explode('/',  $resource);
	while (sizeof($resources_array) > 0)
	{
		$resources[] = "'" . implode('/',$resources_array) . "'";
		array_pop($resources_array);
	}
	$resources[] = "'*'";
	
	$authorizations = $connection->prepare('SELECT `read`, `write`, `create`, `delete` FROM `server`.`authorizations` WHERE user = :user AND owner = :owner AND resource IN (' . implode(',',$resources) . ') ORDER BY length(resource) DESC');
	$authorizations->bindParam(':user', $user_id, PDO::PARAM_INT);
	$authorizations->bindParam(':owner', $owner_id, PDO::PARAM_INT);
	$authorizations->execute();

	if ($authorizations->rowCount() == 0)
		return array('read' => '0', 'write' => '0', 'create' => '0', 'delete' => '0');
	else
		return $authorizations->fetch(PDO::FETCH_ASSOC);
}


function get_user_db($user_id)
{
	global $connection;
	$db = $connection->query("SELECT `email` FROM `server`.`users` WHERE id = '" . $user_id . "'")->fetch(PDO::FETCH_ASSOC);
	if (!$db['email'])
	{
		http_response_code(404);
		die(json_encode(array("code" => 404, "message" =>  "L'utilisateur " . $user_id . " n'existe pas")));
	}
	return $db['email'];
}


function get_user_id($user_db)
{
	global $connection;
	$db = $connection->query("SELECT `id` FROM `server`.`users` WHERE email = '" . $user_db . "'")->fetch(PDO::FETCH_ASSOC);
	if (!$db['id'])
	{
		http_response_code(404);
		die(json_encode(array("code" => 404, "message" =>  "L'utilisateur " . $user_db . " n'existe pas")));
	}
	return $db['id'];
}
?>