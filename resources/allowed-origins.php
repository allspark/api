<?php
function get()
{
	global $connection, $input;
	auth();
	allowed_origins_only();
	admin_only();

	$origins = $connection->query("SELECT id, origin FROM server.allowed_origins")->fetchAll(PDO::FETCH_ASSOC);
	return array("code" => 200, "data" => $origins);
}


function post()
{
	global $connection, $input;
	auth();
	allowed_origins_only();
	admin_only();

	validate('origin', $input->body->origin, 'origin', true);
	
	if (exists($connection, 'server', 'allowed_origins', 'origin', $input->body->origin))
		return array("code" => 409, "message" => "Erreur - Cette origine existe déjà");

	$next_id = $connection->query("SELECT MAX(id) FROM `server`.`allowed_origins`")->fetch(PDO::FETCH_ASSOC);

	$origin = $connection->prepare("INSERT INTO `server`.`allowed_origins` SET id = '" . ($next_id['MAX(id)']+1) . "', origin = '" . strtolower($input->body->origin) . "'");
	if($origin->execute())
		return array("code" => 201, "data" => array('id' => ($next_id['MAX(id)']+1), 'origin' => strtolower($input->body->origin)), "message" => "Origine ajoutée avec succès");
	else
		return array("code" => 400, "message" => $origin->errorInfo()[2]);
}


function patch()
{
	global $connection, $input;
	auth();
	allowed_origins_only();
	admin_only();

	$input->id = $input->path[2];
	validate('id', $input->path[2], 'integer', true);
	validate('origin', $input->body->origin, 'origin', true);
	
	if (exists($connection, 'server','allowed_origins','origin',$input->body->origin))
		return array("code" => 409, "message" => "Erreur - Cette origine existe déjà");

	$origin = $connection->prepare("UPDATE `server`.`allowed_origins` SET origin = '" . strtolower($input->body->origin) . "' WHERE id = '" . $input->id . "'");
	if($origin->execute())
		return array("code" => 200, "data" => array('id' => $input->id, 'origin' => strtoupper($input->body->origin)), "message" => "Origine modifiée avec succès");
	else
		return array("code" => 400, "message" => $origin->errorInfo()[2]);
}


function delete()
{
	global $connection, $input;
	auth();
	allowed_origins_only();
	admin_only();

	$input->id = $input->path[2];
	validate('id', $input->path[2], 'integer', true);
	
	if (!exists($connection, 'server', 'allowed_origins', 'id', $input->id))
		return array("code" => 409, "message" => "Erreur - Cette origine n'existe pas dans la base");
	
	$origin = $connection->prepare("DELETE FROM `server`.`allowed_origins` WHERE id = '" . $input->id . "'");
	if($origin->execute())
		return array("code" => 200, "message" => "Origine supprimée avec succès");
	else
		return array("code" => 400, "message" => $origin->errorInfo()[2]);
}
?>
