<?php
function get()
{
	global $connection, $input;
	auth();
	allowed_origins_only();

	validate('id', $input->path[2], 'integer', false);
	$input->id = $input->path[2];
	
	if (isset($input->id))
	{
		$authorizations = $connection->prepare("SELECT * FROM `server`.`authorizations` WHERE id = '" . $input->id . "'");
		$authorizations->execute();
		$authorizations = $authorizations->fetchAll(PDO::FETCH_ASSOC);
	}
	//RETOURNE LA LISTE DES AUTORISATIONS ACCORDEES PAR LE OWNER
	else if (isset($input->body->owner))
	{	
		validate('owner', $input->body->owner, 'integer', true);
		validate('resource', $input->body->resource, 'resource', false);
		$query = "SELECT `authorizations`.`id`, `user`, `users`.`firstname`, `users`.`lastname`, `users`.`email`, `resource`, `read`, `write`, `create`, `delete` FROM `server`.`authorizations`, `server`.`users` WHERE authorizations.user = users.id AND owner = :owner";
		if (isset($input->body->resource))
			$query .= " AND (resource = :resource OR resource = '*')";
		$authorizations = $connection->prepare($query);
		$authorizations->bindParam(':owner', $input->body->owner, PDO::PARAM_INT);
		if (isset($input->body->resource))
			$authorizations->bindParam(':resource', $input->body->resource, PDO::PARAM_STR);
		$authorizations->execute();
		$authorizations = $authorizations->fetchAll(PDO::FETCH_ASSOC);
		$owner = $connection->query("SELECT id, firstname, lastname, email FROM `server`.`users` WHERE id = '" . $input->body->owner . "'")->fetch(PDO::FETCH_ASSOC);;
		$authorizations[] = array("id"=>"0", "user"=>$owner['id'], "firstname"=>$owner['firstname'], "lastname"=>$owner['lastname'], "email"=>$owner['email'], "resource"=>"*", "read"=>"1", "write"=>"1", "create"=>"1", "delete"=>"1");
		return array("code" => 200, "data" => $authorizations);
	}
	//RETOURNE LA LISTE DES AUTORISATIONS DONT DISPOSE LE USER
	else if (isset($input->body->user))
	{	
		validate('user', $input->body->user, 'integer', true);
		validate('resource', $input->body->resource, 'resource', false);
		$query = "SELECT `authorizations`.`id`, `owner`, `users`.`firstname`, `users`.`lastname`, `users`.`email`, `resource`, `read`, `write`, `create`, `delete` FROM `server`.`authorizations`, `server`.`users` WHERE authorizations.owner = users.id AND user = :user";
		if (isset($input->body->resource))
			$query .= " AND (resource = :resource OR resource = '*')";
		$authorizations = $connection->prepare($query);
		$authorizations->bindParam(':user', $input->body->user, PDO::PARAM_INT);
		if (isset($input->body->resource))
			$authorizations->bindParam(':resource', $input->body->resource, PDO::PARAM_STR);
		$authorizations->execute();
		$authorizations = $authorizations->fetchAll(PDO::FETCH_ASSOC);
		$user = $connection->query("SELECT id, firstname, lastname, email FROM `server`.`users` WHERE id = '" . $input->body->user . "'")->fetch(PDO::FETCH_ASSOC);;
		$authorizations[] = array("id"=>"0", "owner"=>$user['id'], "firstname"=>$user['firstname'], "lastname"=>$user['lastname'], "email"=>$user['email'], "resource"=>"*", "read"=>"1", "write"=>"1", "create"=>"1", "delete"=>"1");
		return array("code" => 200, "data" => $authorizations);
	}
	return array("code" => 400, "message" => "owner ou user doit être renseigné");
}


function post()
{
	global $connection, $input;
	auth();
	allowed_origins_only();

	validate('user', $input->body->user, 'integer', true);
	validate('owner', $input->body->owner, 'integer', true);
	validate('resource', $input->body->resource, 'resource', true);
	validate('read', $input->body->read, 'boolean', true);
	validate('write', $input->body->write, 'boolean', true);
	validate('create', $input->body->create, 'boolean', true);
	validate('delete', $input->body->delete, 'boolean', true);

	if ($input->body->owner != $input->user->id AND !is_admin($input->user->id))
		return array("code" => 401, "message" => "Accès refusé - Seul un administrateur ou le propriétaire de la ressource peut configurer les autorisations");

	$exists = $connection->prepare("SELECT id FROM `server`.`authorizations` WHERE user=:user AND owner=:owner AND resource=:resource");
	$exists->bindParam(':user', $input->body->user, PDO::PARAM_INT);
	$exists->bindParam(':owner', $input->body->owner, PDO::PARAM_INT);
	$exists->bindParam(':resource', $input->body->resource, PDO::PARAM_STR);
	$exists->execute();
	if ($exists->rowCount() > 0)
		return array("code" => 409, "message" => "Erreur - une autorisation pour cette resource et cet utilisateur existe déjà");
	
	if (!exists($connection,'server','users','id',$input->body->user))
		return array("code" => 404, "message" => "user : cet utilisateur n'existe pas");
	
	if (!exists($connection,'server','users','id',$input->body->owner))
		return array("code" => 404, "message" => "owner : cet utilisateur n'existe pas");
	
	$authorizations = $connection->prepare('INSERT INTO `server`.`authorizations` SET `user`=:user, `owner`=:owner, `resource`=:resource, `read`=:read, `write`=:write, `create`=:create, `delete`=:delete');
	$authorizations->bindParam(':user', $input->body->user, PDO::PARAM_INT);
	$authorizations->bindParam(':owner', $input->body->owner, PDO::PARAM_INT);
	$authorizations->bindParam(':resource', $input->body->resource, PDO::PARAM_STR);
	$authorizations->bindParam(':read', $input->body->read, PDO::PARAM_INT);
	$authorizations->bindParam(':write', $input->body->write, PDO::PARAM_INT);
	$authorizations->bindParam(':create', $input->body->create, PDO::PARAM_INT);
	$authorizations->bindParam(':delete', $input->body->delete, PDO::PARAM_INT);

	if($authorizations->execute())
		return array("code" => 201, "data" => array('id'=>$connection->lastInsertId(), 'user'=>$input->body->user, 'owner'=>$input->body->owner, 'resource'=>$input->body->resource, 'read'=>$input->body->read, 'write'=>$input->body->write, 'create'=>$input->body->create, 'delete'=>$input->body->delete), "message" => "Utilisateur créé avec succès");
	else
		return array("code" => 400, "message" => $authorizations->errorInfo()[2]);
}


function patch()
{
	global $connection, $input;
	auth();
	allowed_origins_only();

	validate('id', $input->path[2], 'integer', true);
	$input->id = $input->path[2];

	validate('id', $input->body->id, 'locked', false);
	validate('owner', $input->body->owner, 'locked', false);
	validate('resource', $input->body->resource, 'locked', false);
	validate('user', $input->body->user, 'integer', false);
	validate('read', $input->body->read, 'boolean', false);
	validate('write', $input->body->write, 'boolean', false);
	validate('create', $input->body->create, 'boolean', false);
	validate('delete', $input->body->delete, 'boolean', false);

	$autorisation = $connection->query("SELECT `owner`, `resource` FROM `server`.`authorizations` WHERE id = " . $input->id)->fetch(PDO::FETCH_ASSOC);
	if (sizeof($autorisation) == 0)
		return array("code" => 404, "message" => "Cette autorisation n'existe pas");
	else if ($autorisation['owner'] != $input->user->id AND !is_admin($input->user->id))
		return array("code" => 401, "message" => "Accès refusé - Seul un administrateur ou le propriétaire de la ressource peut configurer les autorisations");

	if (isset($input->body->user))
	{
		if (!exists($connection,'server','users','id',$input->body->user))
			return array("code" => 404, "message" => "user : cet utilisateur n'existe pas");

		$exists = $connection->prepare("SELECT id FROM `server`.`authorizations` WHERE `user`=:user AND `owner`=:owner AND `resource`=:resource AND `id`!=:id");
		$exists->bindParam(':id', $input->id, PDO::PARAM_INT);
		$exists->bindParam(':user', $input->body->user, PDO::PARAM_INT);
		$exists->bindParam(':owner', $autorisation['owner'], PDO::PARAM_INT);
		$exists->bindParam(':resource', $autorisation['resource'], PDO::PARAM_STR);
		$exists->execute();
		if ($exists->rowCount() > 0)
			return array("code" => 409, "message" => "Erreur - une autorisation pour cette resource et cet utilisateur existe déjà");
	}

	$query = "UPDATE `server`.`authorizations` SET ";
	foreach($input->body as $key => $value)
		if ($key!='id' AND $key!='owner' AND $key!='resource')
			$query .= '`'.$key.'`=:'.$key.',';
	$query = substr($query,0,-1);
	$query .= " WHERE id = '" . $input->id . "'";

	$authorization = $connection->prepare($query);
	foreach($input->body as $key => $value)
		if ($key!='id' AND $key!='owner' AND $key!='resource')
			$authorization->bindParam(':'. $key, $input->body->$key, $input->fields->$key);
		
	if($authorization->execute())
	{
		$updated_authorization = $connection->query("SELECT * FROM `server`.`authorizations` WHERE id = " . $input->id)->fetch(PDO::FETCH_ASSOC);
		return array("code" => 200, "data" => $updated_authorization, "message" => "Autorisation modifiée avec succès");
	}
	else
		return array("code" => 400, "message" => $updated_authorization->errorInfo()[2]);
}

function delete()
{
	global $connection, $input;
	auth();
	allowed_origins_only();

	validate('id', $input->path[2], 'integer', false);
	$input->id = $input->path[2];

	$authorization = $connection->query("SELECT `owner` FROM `server`.`authorizations` WHERE id = " . $input->id)->fetch(PDO::FETCH_ASSOC);
	if (sizeof($authorization) == 0)
		return array("code" => 404, "message" => "Cette autorisation n'existe pas");
	else if ($authorization['owner'] != $input->user->id AND !is_admin($input->user->id))
		return array("code" => 401, "message" => "Accès refusé - Seul un administrateur ou le propriétaire de la ressource peut configurer les autorisations");
	
	$delete = $connection->prepare("DELETE FROM `server`.`authorizations` WHERE id = '" . $input->id . "'");
	if($delete->execute())
		return array("code" => 200, "message" => "Autorisation supprimée avec succès");
	else
		return array("code" => 400, "message" => $authorizations->errorInfo()[2]);
	
}
?>