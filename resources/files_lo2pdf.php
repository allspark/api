<?php
function get()
{
	global $connection, $input;
	auth();
	allowed_origins_only();

	$file = explode('/',$input->body->file);
	$input->owner = new stdClass;
	$input->owner->id = get_user_id($file[2]);
	$input->owner->db = $file[2];

	validate('owner_id', $input->owner->id, 'integer', true);
	validate('owner_db', $input->owner->db, 'email', true);
	
	$authorizations = get_rights($input->user->id, $input->owner->id, $input->body->file);
	if ($authorizations['read'] == 0 OR $authorizations['write'] == 0 OR $authorizations['create'] == 0)
		return array("code" => 403, "message" => "Vous n'avez pas les autorisations suffisantes pour effectuer cette action");

	if (!is_file('/srv' . $input->body->file))
		return array("code" => 404, "message" => "Le fichier spécifié n'existe pas");
	
	if (is_file('/srv' . substr($input->body->file,0,strrpos($input->body->file,'.')) . '.pdf'))
		return array("code" => 409, "message" => "Le fichier de destination existe déjà");

	$path = str_replace('/files/'. $input->owner->db . '/', '', $input->body->file);
	exec("LC_ALL=fr_FR.UTF-8 export HOME=/srv/api/tmp && libreoffice --headless --convert-to pdf '/srv" . $input->body->file . "' --outdir '/srv" . substr($input->body->file,0,strrpos($input->body->file,'/')) . "' 2>&1",$output);

	if (is_file('/srv' . substr($input->body->file,0,strrpos($input->body->file,'.')) . '.pdf'))
		return array("code" => 200, "message" => "Conversion effectuée avec succès");
	else
		return array("code" => 400, "message" => "La conversion a échoué");
}
?>
