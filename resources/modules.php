<?php
function get()
{
	global $connection, $input;
	allowed_origins_only();

	$modules = $connection->prepare("SELECT modules_groups.name as group_name, modules_groups.displayname as group_displayname, modules.name, modules.displayname, modules.admin_only, modules.disabled, modules.link, modules.icon FROM `server`.`modules`, `server`.`modules_groups` WHERE modules_groups.name = modules.group ORDER BY modules_groups.position, modules.position");

	if($modules->execute())
	{
		$x=-1; $y=0; $last_group_name = '';
		while ($module = $modules->fetch(PDO::FETCH_ASSOC))
		{
			if ($module['group_name'] != $last_group_name)
			{
				$last_group_name = $module['group_name'];
				$x++;$y=0;
				$results['groups'][$x]['name'] = $module['group_name'];
				$results['groups'][$x]['displayname'] = $module['group_displayname'];
			}
			
			$results['groups'][$x]['modules'][$y]['name'] = $module['name'];
			$results['groups'][$x]['modules'][$y]['displayname'] = $module['displayname'];
			$results['groups'][$x]['modules'][$y]['admin_only'] = $module['admin_only'];
			$results['groups'][$x]['modules'][$y]['disabled'] = $module['disabled'];
			$results['groups'][$x]['modules'][$y]['link'] = $module['link'];
			$results['groups'][$x]['modules'][$y]['icon'] = $module['icon'];
			
			$y++;
		}
			
		return array("code" => 200, "data" => $results);
	}
	else
		return array("code" => 400, "message" => $preferences->errorInfo()[2]);
}
?>