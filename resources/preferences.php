<?php
function get()
{
	global $connection, $input;
	auth();
	allowed_origins_only();

	validate('user', $input->body->user, 'integer', true);
	validate('owner', $input->body->owner, 'integer', true);
	validate('module', $input->body->module, 'module', false);
	validate('preference', $input->body->preference, 'module', false);

	if ($input->body->user != $input->user->id AND !is_admin($input->user->id))
		return array("code" => 401, "message" => "Accès refusé - Seul l'utilisateur lui même, ou un administrateur, peut accéder à ces préférences");

	$query = "SELECT `module`, `preference`, `value` FROM `server`.`preferences` WHERE `owner`=:owner AND `user`=:user";
	if (isset($input->body->module))
		$query .= " AND `module`=:module";
	if (isset($input->body->preference))
		$query .= " AND `preference`=:preference";
	$query .= " ORDER BY `module`, `preference`";

	$preferences = $connection->prepare($query);
	$preferences->bindParam(':owner', $input->body->owner, PDO::PARAM_INT);
	$preferences->bindParam(':user', $input->body->user, PDO::PARAM_INT);
	if (isset($input->body->module))
		$preferences->bindParam(':module', $input->body->module, PDO::PARAM_STR);
	if (isset($input->body->preference))
		$preferences->bindParam(':preference', $input->body->preference, PDO::PARAM_STR);
	
	if($preferences->execute())
	{
		while ($preference = $preferences->fetch(PDO::FETCH_ASSOC))
			$results[$preference['module']][$preference['preference']] = json_decode($preference['value']);
		return array("code" => 200, "data" => $results);
	}
	else
		return array("code" => 400, "message" => $preferences->errorInfo()[2]);
}


function post()
{
	global $connection, $input;
	auth();
	allowed_origins_only();
	
	validate('user', $input->body->user, 'integer', true);
	validate('owner', $input->body->owner, 'integer', true);
	validate('module', $input->body->module, 'module', true);
	validate('preference', $input->body->preference, 'module', true);
	validate('value', $input->body->value, 'json', true);

	if ($input->body->user != $input->user->id AND !is_admin($input->user->id))
		return array("code" => 401, "message" => "Accès refusé - une préférence liée à un utilisateur ne peut être créée que par un administrateur ou l'utilisateur lui même");
	
	if ($input->body->user != $input->user->id)
		if (!exists($connection,'server','users','id',$input->body->user))
			return array("code" => 404, "message" => "user : cet utilisateur n'existe pas");
	
	if ($input->body->owner != $input->user->id)
		if (!exists($connection,'server','users','id',$input->body->owner))
			return array("code" => 404, "message" => "owner : cet utilisateur n'existe pas");
	
	if (is_object($input->body->value))
		foreach ($input->body->value as $key => $value)
		{
			$preference = $connection->prepare("REPLACE INTO `server`.`preferences` SET `owner`=:owner, `user`=:user, `module`=:module, `preference`=:preference, `value`=:value");
			$preference->bindParam(':owner', $input->body->owner, PDO::PARAM_INT);
			$preference->bindParam(':user', $input->body->user, PDO::PARAM_INT);
			$preference->bindParam(':module', $input->body->module, PDO::PARAM_STR);
			$preference->bindParam(':preference', $key, PDO::PARAM_STR);
			$preference->bindParam(':value', json_encode($value), PDO::PARAM_STR);
			if(!$preference->execute())
				return array("code" => 400, "message" => $preference->errorInfo()[2]);
		}
	else
	{
		$preference = $connection->prepare("REPLACE INTO `server`.`preferences` SET `owner`=:owner, `user`=:user, `module`=:module, `preference`=:preference, `value`=:value");
		$preference->bindParam(':owner', $input->body->owner, PDO::PARAM_INT);
		$preference->bindParam(':user', $input->body->user, PDO::PARAM_INT);
		$preference->bindParam(':module', $input->body->module, PDO::PARAM_STR);
		$preference->bindParam(':preference', $input->body->preference, PDO::PARAM_STR);
		$preference->bindParam(':value', $input->body->value, PDO::PARAM_STR);
		if(!$preference->execute())
			return array("code" => 400, "message" => $preference->errorInfo()[2]);
	}
	return array("code" => 201);
}


function delete()
{
	global $connection, $input;
	auth();
	allowed_origins_only();

	validate('user', $input->body->user, 'integer', true);
	validate('owner', $input->body->owner, 'integer', true);
	validate('module', $input->body->module, 'module', false);
	validate('preference', $input->body->preference, 'module', false);

	if ($input->body->user != $input->user->id AND !is_admin($input->user->id))
		return array("code" => 401, "message" => "Accès refusé - une préférence liée à un utilisateur ne peut être supprimée que par un administrateur ou l'utilisateur lui même");
	
	$query = "DELETE FROM `server`.`preferences` WHERE `owner`=:owner AND `user`=:user";
	if (isset($input->body->module))
		$query .= " AND `module`=:module";
	if (isset($input->body->preference))
		$query .= " AND `preference`=:preference";
	
	$delete = $connection->prepare($query);
	$delete->bindParam(':owner', $input->body->owner, PDO::PARAM_INT);
	$delete->bindParam(':user', $input->body->user, PDO::PARAM_INT);
	if (isset($input->body->module))
		$delete->bindParam(':module', $input->body->module, PDO::PARAM_STR);
	if (isset($input->body->preference))
		$delete->bindParam(':preference', $input->body->preference, PDO::PARAM_STR);

	if($delete->execute())
		return array("code" => 200, "message" => "Préférence supprimée avec succès");
	else
		return array("code" => 400, "message" => $delete->errorInfo()[2]);
}
?>