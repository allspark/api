<?php
function get()
{
	global $connection, $input;
	auth();
	allowed_origins_only();

	$services = $connection->query("SELECT * FROM `server`.`services` WHERE status = 1 ORDER BY name")->fetchAll(PDO::FETCH_ASSOC);
	for ($i=0; $i < sizeof($services); $i++)
		$services[$i]['version'] = trim(file('/srv/api/api_' . $services[$i]['name'] . '/VERSION')[1]);

	return array("code" => 200, "data" => $services);
}
?>