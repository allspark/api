<?php
function get_status($app)
{
	exec('systemctl status ' . $app, $output);
	foreach ($output as $line)
	if (strpos($line, 'Active: active') !== false)
		return 'active';
	return 'inactive';
}

function get_version($app)
{
  return shell_exec ("dpkg -s " . $app . " | grep '^Version:' | cut -c 10- | cut -f1 -d'-' | cut -f1 -d'+' | cut -f2 -d':' | head -c -1");
}

function get()
{
	auth();
	allowed_origins_only();
	
	$status['services'][0]['type'] = 'Kernel';
	$status['services'][0]['name'] = shell_exec ("uname -s");
	$status['services'][0]['version'] = shell_exec ("uname -r");
	$status['services'][0]['status'] = 'active';

	$status['services'][1]['type'] = 'Système d\'exploitation (OS)';
	$status['services'][1]['name'] = shell_exec ('env -i bash -c \'. /etc/*-release; echo $NAME\'');
	$status['services'][1]['version'] = shell_exec ('env -i bash -c \'. /etc/*-release; echo $VERSION\'');
	$status['services'][1]['status'] = 'active';

	$status['services'][2]['type'] = 'Serveur web';
	$status['services'][2]['name'] = 'Apache';
	$status['services'][2]['version'] = get_version('apache2');
	$status['services'][2]['status'] = get_status('apache2');

	$status['services'][3]['type'] = 'Language de programmation';
	$status['services'][3]['name'] = 'PHP';
	$status['services'][3]['version'] = get_version('php');
	$status['services'][3]['status'] = ($status['services'][3]['version'] != '')?'active':'inactive';

	$status['services'][4]['type'] = 'Serveur de bases de données';
	$status['services'][4]['name'] = 'Maria DB';
	$status['services'][4]['status'] = get_status('mariadb');
	$status['services'][4]['version'] = get_version('mariadb-server');

	$status['services'][5]['type'] = 'Agent de transfert de courriels';
	$status['services'][5]['name'] = 'Postfix';
	$status['services'][5]['status'] = get_status('postfix');
	$status['services'][5]['version'] = get_version('postfix');

	$status['services'][6]['type'] = 'Serveur mail IMAP';
	$status['services'][6]['name'] = 'Dovecot';
	$status['services'][6]['status'] = get_status('dovecot');
	$status['services'][6]['version'] = get_version('dovecot-core');

	$status['services'][7]['type'] = 'Antispam Mail';
	$status['services'][7]['name'] = 'SpamAssassin';
	$status['services'][7]['status'] = get_status('spamassassin');
	$status['services'][7]['version'] = get_version('spamassassin');

	$status['services'][8]['type'] = 'Antispam Mail (connecteur)';
	$status['services'][8]['name'] = 'Spamassassin Milter';
	$status['services'][8]['status'] = get_status('spamass-milter');
	$status['services'][8]['version'] = get_version('spamass-milter');

	$status['services'][9]['type'] = 'Antivirus mail';
	$status['services'][9]['name'] = 'Clamav';
	$status['services'][9]['status'] = get_status('clamav-daemon');
	$status['services'][9]['version'] = get_version('clamav-daemon');

	$status['services'][10]['type'] = 'Antivirus mail (connecteur)';
	$status['services'][10]['name'] = 'Clamav Milter';
	$status['services'][10]['status'] = get_status('clamav-milter');
	$status['services'][10]['version'] = get_version('clamav-milter');

	$status['services'][11]['type'] = 'Logiciel de sauvegardes différentielles';
	$status['services'][11]['name'] = 'Rdiff Backup';
	$status['services'][11]['status'] = get_status('rdiff-backup');
	$status['services'][11]['version'] = get_version('rdiff-backup');

	$status['services'][12]['type'] = 'Générateur de certificats SSL';
	$status['services'][12]['name'] = 'Certbot';
	$status['services'][12]['version'] = get_version('certbot');
	$status['services'][12]['status'] = ($status['services'][12]['version'] != '')?'active':'inactive';
	$status['services'][12]['other'] = "Expiration : " . date('d/m/Y',strtotime(substr(shell_exec("openssl s_client -servername 'devoptimus.ovh' -connect 'devoptimus.ovh:443' | openssl x509 -noout -enddate | grep 'notAfter='"),9)));

	$status['services'][13]['type'] = 'Webmail';
	$status['services'][13]['name'] = 'Roundcube Webmail';
	$status['services'][13]['version'] = shell_exec ("cat /srv/webmail/CHANGELOG.md | grep 'Release' | head -1 | cut -c 12- | head -c -1");
	$status['services'][13]['status'] = ($status['services'][13]['version'] != '')?'active':'inactive';

	$status['services'][14]['type'] = 'Serveur de fichiers WEBDAV';
	$status['services'][14]['name'] = 'SabreDAV';
	$status['services'][14]['version'] = shell_exec ("cat /srv/cloud/vendor/sabre/dav/lib/DAV/Version.php | grep VERSION | cut -f2 -d \"'\"");
	$status['services'][14]['status'] = ($status['services'][14]['version'] != '')?'active':'inactive';

	$status['services'][15]['type'] = 'Interface de Programmation';
	$status['services'][15]['name'] = 'API allspark';
	$status['services'][15]['status'] = 'active';
	exec('cat /srv/api/api_allspark/VERSION', $version);
	$status['services'][15]['version'] = $version[1];
	$status['services'][15]['other'] = $version[0];

	$status['services'][16]['type'] = 'Interface de Programmation';
	$status['services'][16]['name'] = 'API optimus-avocats';
	$status['services'][16]['status'] = 'active';
	exec('cat /srv/api/api_optimus-avocats/VERSION', $version);
	$status['services'][16]['version'] = $version[1];
	$status['services'][16]['other'] = $version[0];

	$status['services'][17]['type'] = 'Interface de Programmation';
	$status['services'][17]['name'] = 'API optimus-structures';
	$status['services'][17]['status'] = 'active';
	exec('cat /srv/api/api_optimus-avocats/VERSION', $version);
	$status['services'][17]['version'] = $version[1];
	$status['services'][17]['other'] = $version[0];

	$status['cpu']['usage'] =  floatval(shell_exec("top -n 1 -b | awk '/^%Cpu/{print $2}'"));

	$status['memory']['usage'] = floatval(shell_exec("free | grep Mem | awk '{print $3/$2 * 100.0}'"));

	$system_disk = preg_split('/\s+/',shell_exec("df --output=source,target,used,size,pcent | grep ' / '"));
	$status['disk'][0]['name'] = "System Disk";
	$status['disk'][0]['device'] = $system_disk[0];
	$status['disk'][0]['mount'] = $system_disk[1];
	$status['disk'][0]['used'] = $system_disk[2];
	$status['disk'][0]['total'] = $system_disk[3];
	$status['disk'][0]['usage'] = $system_disk[4];

	$data_disk = preg_split('/\s+/',shell_exec("df --output=source,target,used,size,pcent | grep '/srv'"));
	$status['disk'][1]['name'] = "Data Disk";
	$status['disk'][1]['device'] = $data_disk[0];
	$status['disk'][1]['mount'] = $data_disk[1];
	$status['disk'][1]['used'] = $data_disk[2];
	$status['disk'][1]['total'] = $data_disk[3];
	$status['disk'][1]['usage'] = $data_disk[4];

	$status['freespace'] = intval(shell_exec("lsblk -bno SIZE /dev/sda | head -1")) / 1024 - $status['disk'][0]['total'] - $status['disk'][1]['total'];

	return array("code" => 200, "data" => $status);
}
?>
