<?php
function get()
{
	global $connection, $input;
	auth();
	allowed_origins_only();

	validate('id', $input->path[2], 'integer', false);
	$input->id = $input->path[2];
	
	if (@$input->id)
		$users = $connection->query("SELECT id, server, status, admin, email, firstname, lastname, '' as password FROM `server`.`users` WHERE id = " . $input->id)->fetchAll(PDO::FETCH_ASSOC);
	else
		$users = $connection->query("SELECT id, server, status, admin, email, firstname, lastname, '' as password FROM `server`.`users`")->fetchAll(PDO::FETCH_ASSOC);

	return array("code" => 200, "data" => @$input->id?$users[0]:$users);
}

function post()
{
	global $connection, $input;
	auth();
	allowed_origins_only();
	admin_only();

	validate('id', $input->path[2], 'integer', false);
	$input->id = $input->path[2];

	$input->mutables = array('server','email','status','admin','password','firstname','lastname');
	validate('server', $input->body->server, 'domain', true);
	validate('email', $input->body->email, 'email', true);
	validate('status', $input->body->status, 'boolean', true);
	validate('admin', $input->body->admin, 'boolean', true);
	validate('password', $input->body->password, 'password', false);
	validate('firstname', $input->body->firstname, 'string', false);
	validate('lastname', $input->body->lastname, 'string', false);

	if (isset($input->body->email))
		$input->body->email = strtolower($input->body->email);

	if (exists($connection, 'server','users', 'email', $input->body->email))
		return array("code" => 409, "message" => "Erreur - un utilisateur avec cette adresse email existe déjà");
	
	include 'config.php';
	$input->body->password = base64_decode(openssl_encrypt($input->body->password, 'aes-128-ecb', $aes_key));

	$query = "INSERT INTO `server`.`users` SET ";
	foreach($input->body as $key => $value)
		if (in_array($key,$input->mutables))
			$query .= $key.'=:'.$key.',';
	$query = substr($query,0,-1);

	$user = $connection->prepare($query);
	foreach($input->body as $key => $value)
		if (in_array($key,$input->mutables))
			$user->bindParam(':'. $key, $input->body->$key, $input->fields[$key]);
	
	if($user->execute())
	{
		$new_id = $connection->lastInsertId();
		$newmailbox = $connection->query("INSERT INTO `mailserver`.`mailboxes` SET id = " . $new_id . ", email='" . $input->body->email . "', quota=0, status=1")->fetch(PDO::FETCH_ASSOC);
		umask(0);
		@mkdir('/srv/mailboxes/' . $input->body->email, 0770);
		@chmod('/srv/mailboxes/' . $input->body->email, 0770);
		@chgrp('/srv/mailboxes/' . $input->body->email, 'mailboxes');
		@touch('/srv/mailboxes/' . $input->body->email . '/subscriptions');
		@chmod('/srv/mailboxes/' . $input->body->email . '/subscriptions', 0770);
		@chgrp('/srv/mailboxes/' . $input->body->email . '/subscriptions', 'mailboxes');
		$new_user = $connection->query("SELECT id, status, admin, email, firstname, lastname FROM `server`.`users` WHERE id = " . $new_id)->fetch(PDO::FETCH_ASSOC);
		return array("code" => 201, "data" => $new_user, "message" => "Utilisateur créé avec succès");
	}
	else
		return array("code" => 400, "message" => $user->errorInfo()[2]);
}

function patch()
{
	global $connection, $input;
	auth();
	allowed_origins_only();

	validate('id', $input->path[2], 'integer', false);
	$input->id = $input->path[2];

	$input->mutables = array('server','status','admin','password','firstname','lastname');
	if (!isset($input->body)) return array("code" => 400, "message" => "Aucune donnée à modifier n'a été transmise");
	validate('server', $input->body->server, 'domain', false);
	validate('status', $input->body->status, 'boolean', false);
	validate('admin', $input->body->admin, 'boolean', false);
	validate('password', $input->body->password, 'password', false);
	validate('firstname', $input->body->firstname, 'string', false);
	validate('lastname', $input->body->lastname, 'string', false);

	if ($input->id != $input->user->id AND !is_admin($input->user->id))
		return array("code" => 401, "message" => "Accès refusé - fonction réservée aux administrateurs");

	if (isset($input->body->password))
	{
		include 'config.php';
		$input->body->password = base64_decode(openssl_encrypt($input->body->password, 'aes-128-ecb', $aes_key));
	}

	if (isset($input->body->email))
		$input->body->email = strtolower($input->body->email);

	if (!exists($connection, 'server','users', 'id', $input->id))
		return array("code" => 409, "message" => "Erreur - cet utilisateur n'existe pas");
	
	if (exists($connection, 'server','users', 'email', $input->body->email))
		return array("code" => 409, "message" => "Erreur - un utilisateur avec cette adresse email existe déjà");
	
	$query = "UPDATE `server`.`users` SET ";
	foreach($input->body as $key => $value)
		if (in_array($key,$input->mutables))
			$query .= $key.'=:'.$key.',';
	$query = substr($query,0,-1);
	$query .= " WHERE id = '" . $input->id . "'";

	$user = $connection->prepare($query);
	foreach($input->body as $key => $value)
		if (in_array($key,$input->mutables))
			$user->bindParam(':'. $key, $input->body->$key, $input->fields[$key]);
	
	if($user->execute())
	{
		$modified_user = $connection->query("SELECT id, status, admin, email, firstname, lastname FROM `server`.`users` WHERE id = " . $input->id)->fetch(PDO::FETCH_ASSOC);
		return array("code" => 200, "data" => $modified_user, "message" => "Utilisateur modifié avec succès");
	}
	else
		return array("code" => 400, "message" => $user->errorInfo()[2]);
}

function delete()
{
	global $connection, $input;
	auth();
	allowed_origins_only();
	admin_only();

	validate('id', $input->path[2], 'integer', true);
	$input->id = $input->path[2];
	$input->db = get_user_db($input->path[2]);

	if (is_dir('/srv/files/' . $input->db))
		exec('rm -r /srv/files/' . $input->db);
	
	if (is_dir('/srv/mailboxes/' . $input->db))
		exec('rm -r /srv/mailboxes/' . $input->db);
	
	$delete = $connection->prepare("DELETE FROM `server`.`authorizations` WHERE user = '" . $input->id . "' OR owner = '" . $input->id . "'");
	if (!$delete->execute()) return array("code" => 400, "message" => $delete->errorInfo()[2]);
	$delete = $connection->prepare("DELETE FROM `server`.`preferences` WHERE user = '" . $input->id . "' OR owner = '" . $input->id . "'");
	if (!$delete->execute()) return array("code" => 400, "message" => $delete->errorInfo()[2]);
	$delete = $connection->prepare("DELETE FROM `mailserver`.`mailboxes` WHERE id = '" . $input->id . "'");
	if (!$delete->execute()) return array("code" => 400, "message" => $delete->errorInfo()[2]);
	$delete = $connection->prepare("DELETE FROM `server`.`users` WHERE id = '" . $input->id . "'");
	if (!$delete->execute()) return array("code" => 400, "message" => $delete->errorInfo()[2]);
	$delete = $connection->prepare("DELETE FROM `server`.`users_services` WHERE user = '" . $input->id . "'");
	if (!$delete->execute()) return array("code" => 400, "message" => $delete->errorInfo()[2]);
	$delete = $connection->prepare("DELETE FROM `server`.`users_servers` WHERE user = '" . $input->id . "'");
	if (!$delete->execute()) return array("code" => 400, "message" => $delete->errorInfo()[2]);
	$delete = $connection->prepare("DROP DATABASE IF EXISTS `" . $input->db . "`");
	if (!$delete->execute()) return array("code" => 400, "message" => $delete->errorInfo()[2]);
	return array("code" => 200);
}
?>
