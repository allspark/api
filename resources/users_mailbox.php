<?php
function get()
{
	global $connection, $input;
	auth();
	allowed_origins_only();

	validate('id', $input->path[2], 'integer', true);
	$input->id = $input->path[2];

	if ($input->id != $input->user->id AND !is_admin($input->user->id))
		return array("code" => 401, "message" => "Accès refusé - fonction réservée aux administrateurs");

	$mailbox = $connection->query("SELECT status, quota, aliases, redirections, sender_bcc, recipient_bcc FROM mailserver.mailboxes WHERE id = " . $input->id)->fetchAll(PDO::FETCH_ASSOC);
	return array("code" => 200, "data" => $mailbox[0]);
}


function patch()
{
	global $connection, $input;
	auth();
	allowed_origins_only();

	validate('id', $input->path[2], 'integer', true);
	$input->id = $input->path[2];

	if ($input->id != $input->user->id AND !is_admin($input->user->id))
		return array("code" => 401, "message" => "Accès refusé - fonction réservée aux administrateurs");
	
	if (isset($input->body->quota) AND !is_admin($input->user->id))
		return array("code" => 401, "message" => "Seul un administrateur peut modifier le quota");
	
	$input->mutables = array('status','aliases','redirections','sender_bcc','recipient_bcc','quota');
	validate('status', $input->body->status, 'boolean', false);
	validate('aliases', $input->body->aliases, 'email_list', false);
	validate('redirections', $input->body->redirections, 'email_list', false);
	validate('sender_bcc', $input->body->sender_bcc, 'email_list', false);
	validate('recipient_bcc', $input->body->recipient_bcc, 'email_list', false);
	validate('quota', $input->body->quota, 'decimal', false);
		
	$array = array('aliases','redirections','sender_bcc','recipient_bcc');
	foreach($array as $field)
		if (isset($input->body->$field) and $input->body->$field != '')
		{
			$input->body->$field = strtolower($input->body->$field);
			$emails = explode(';',$input->body->$field);
			foreach($emails as $email)
				if ($field=='aliases' AND exists($connection, 'mailserver', 'mailboxes', 'email', $email)==true)
					return array("code" => 409, "message" => "Erreur - l'adresse $email ne peut pas être utilisée comme alias car elle appartient déjà à un autre utilisateur");
		}
	
	$query = "UPDATE mailserver.mailboxes SET ";
	foreach($input->body as $key => $value)
		if (in_array($key,$input->mutables))
			$query .= $key.'=:'.$key.',';
	$query = substr($query,0,-1);
	$query .= " WHERE id = '" . $input->id . "'";

	$mailbox = $connection->prepare($query);
	foreach($input->body as $key => $value)
		if (in_array($key,$input->mutables))
			$mailbox->bindParam(':'. $key, $input->body->$key, $input->fields[$key]);
	
	if($mailbox->execute())
	{
		$new_mailbox = $connection->query("SELECT status, quota, aliases, redirections, sender_bcc, recipient_bcc FROM mailserver.mailboxes WHERE id = " . $input->id)->fetch(PDO::FETCH_ASSOC);
		return array("code" => 200, "data" => $new_mailbox, "message" => "Boite mail modifiée avec succès");
	}
	else
		return array("code" => 400, "message" => $mailbox->errorInfo()[2]);
}
?>
