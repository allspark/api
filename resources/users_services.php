<?php
function get()
{
	global $connection, $input;
	auth();
	allowed_origins_only();

	validate('id', $input->path[2], 'integer', true);
	$input->id = $input->path[2];

	$services = $connection->query("SELECT name, displayname FROM `server`.`services`, `server`.`users_services` WHERE `users_services`.`service` = `services`.`name` AND `users_services`.user = '" . $input->id . "'")->fetchAll(PDO::FETCH_ASSOC);
	for ($i=0; $i < sizeof($services); $i++)
		$services[$i]['version'] = trim(file('/srv/api/api_' . $services[$i]['name'] . '/VERSION')[1]);
		
	return array("code" => 200, "data" => $services);
}
?>